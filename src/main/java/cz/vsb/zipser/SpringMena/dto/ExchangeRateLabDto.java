package cz.vsb.zipser.SpringMena.dto;

public class ExchangeRateLabDto {
	private ExchangeRateLabRateDto rate;
	private String baseCurrency;
	private Long timeStamp;
	
	public ExchangeRateLabRateDto getRate() {
		return rate;
	}
	public void setRate(ExchangeRateLabRateDto rate) {
		this.rate = rate;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	

}
