package cz.vsb.zipser.SpringMena.dto;

public class ExchangeRateLabRateDto {
	private Double rate;
	private String to;
	
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	

}
