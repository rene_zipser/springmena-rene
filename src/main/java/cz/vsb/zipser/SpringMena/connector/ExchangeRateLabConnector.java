package cz.vsb.zipser.SpringMena.connector;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import cz.vsb.zipser.SpringProject.dto.WundergroundDto;

public class ExchangeRateLabConnector {
	private static String baseURL = "http://api.exchangeratelab.com/";
	private static String APIkey = "599D455EF60E9C712BBCD9A9F5F14661";
	private static String urlParams = "current?apikey=";
	String url = baseURL + urlParams + APIkey;
	RestTemplate template = new RestTemplate();
	URI uri = new URI (url);
	

	public ExchangeRateLabConnector(RestTemplate template) {
		super();
		this.template = template;
	}

	public void getExchangeRate(){}
	
	ResponseEntity<WundergroundDto> response = template.getForEntity(uri, WundergroundDto.class);

	return response.getBody().getCurrent_observation().getDisplay_location().getCity().toString();
}
